REVIEWS BEAVER BUILDER MODULE
=============================

A beaver builder module for encouraging positive reviews on popular review sites while directing negative reviews to a private comment form.
